document.addEventListener("deviceready", startApp, false);

function startApp(){

  FastClick.attach(document.body);

  $('#menu-btn').click(function(e){
  	e.preventDefault();
  	if($('#left').hasClass('center')){
      $('#left').removeClass('center').addClass('left');
  	}else{
      $('#left').removeClass('left').addClass('center');
  	  $('#right').removeClass('center').addClass('right');
  	}
  	
  });


  $('.link-ajax').click(function(e){
  	e.preventDefault();
  	var link = $(this).attr('href');
  	
    $.ajax({
  	  url : link,
  	  context: document.body,
  	  beforeSend: function(){
        loader(true);
      },
      success: function(data){
      	$('#container').html(data);
    	loader(false);
      }
    })  
  });

  $('.remote').click(function(e){
  	e.preventDefault();
  	 $.ajax({
  	  url : 'http://www.marcelsafont.com/enviar.php',
  	  context: document.body,
  	  beforeSend: function(){
        loader(true);
      },
      success: function(data){
      	$('#container').html(data);
    	loader(false);
      }
    })
  })
}

function loader(status){
	if(status == true){
		$('#left').removeClass('center').addClass('left');
		$('#loader').addClass('show');
	}
	else{
		$('#loader').removeClass('show');
	}
	
}
